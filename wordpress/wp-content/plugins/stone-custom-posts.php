<?php
/**
 * Plugin Name: Location Custom Posts
 * Description: Adds the 'locations' custom post type
 * Version: 0.1
 * Author: Jonas Ermen
 * Author URI: http://www.otherday.net
 * License: GPL2
 */

defined('ABSPATH') or die();

// Register Custom Post Type
function locations_post_type() {

	$location_labels = array(
		'name'               => 'Locations',
		'singular_name'      => 'Location',
	);

	$args = array(
		'labels'             => $location_labels,
		'public'             => true,
		'menu_position'      => 5,
		'menu_icon'      	 => 'dashicons-format-image',
		'supports'           => array( 'title', 'editor'),
	);

	register_post_type( 'location', $args );

}

// Hook into the 'init' action
add_action( 'init', 'locations_post_type', 0 );

