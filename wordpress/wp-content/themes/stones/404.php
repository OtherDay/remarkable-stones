
<?php get_header( "static"); ?>

<div class="ruler clearfix"></div>

<div class="container-fluid">




	<div class="row slider-404">
		<div class="col-xs-12">
			<div class="pos-rel">	
				<div class="latlong-overlay">
					<?php get_template_part( 'latlong-404' ); ?>
				</div>
				<?php do_action('slideshow_deploy', 'page 404'); ?>	
			</div>
		</div>
	</div>

	
</div>

<?php get_footer('static'); ?>



