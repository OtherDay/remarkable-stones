<?php
/**
 * The sidebar containing the main widget area.
 *
 * @package Remarkable Stones
 */

?>
<div class="row">
	<div class="col-12 col-sm-6 col-md-12">
		<h4 class="map-toggle">Your stone was found here</h4>

		<?php 
		$latitude = get_field('map_latitude');
		$longitude = get_field('map_longitude');

		
		if( !empty($location)):	?>
			<div class="acf-map" data-desc="<?php echo get_the_title(); ?> " data-lat="<?php echo $latitude; ?>" data-lng="<?php echo $longitude; ?>">

			</div>
		<?php endif; ?>
		

		<div class="pull-right">
			<button class="btn btn-default map-toggler">Expand Map</button>
		</div>

	</div>
	<div class="col-12 col-sm-6 col-md-12">
		<h4>Points of interest</h4>

		<p><?php echo get_field('location_information') ?></p>
	</div>
</div>
