<?php
/**
 * The template for displaying all stone posts.
 *
 * @package Remarkable Stones
 */

get_header('static'); ?>

<div class="ruler clearfix"></div>

<div class="img-responsive header-image pos-rel">
	<img src="<?php echo get_field('main-image')['url'] ?>">
	<div class="shadow-left"></div>
	<div class="shadow-right"></div>
</div>

<div class="container-fluid location-body">

	<div class="location-banner">

		<h2><?php echo get_field('title_message') ?></h2>

		<?php while ( have_posts() ) : the_post(); ?>
			<?php the_title('<h1 class="location-title">', '</h1>'); ?>
		<?php endwhile; // end of the loop. ?>
		<hr>
	</div>

	<div class="row">
		<div class="col-12 col-xs-offset-1 col-xs-10 col-sm-offset-1 col-sm-10 col-lg-offset-2 col-lg-8 map-container map-toggle">
			<?php 
				$latitude = get_field('map_latitude');
				$longitude = get_field('map_longitude');

				if( !empty($location)):	?>
				<div class="acf-map" type="satellite" data-desc="<?php echo get_the_title(); ?> " data-lat="<?php echo $latitude; ?>" data-lng="<?php echo $longitude; ?>"></div>
			<?php endif; ?>
			<div class="pull-right">
				<button class="btn btn-default map-toggler">Toggle Map</button>
			</div>
		</div>
		<div class="col-xs-12 col-md-8 map-toggle">
			<main>
				<?php while ( have_posts() ) : the_post(); ?>
					<?php the_content(); ?>
				<?php endwhile; // end of the loop. ?>
			</main>
		</div> <!-- #col -->
		<div class="col-xs-12 col-md-4 map-toggle">
			<div class="location-sidebar">
				<?php get_sidebar('location'); ?>	
			</div>
		</div> <!-- #sidebar col -->
	</div> <!-- #row -->
</div> <!-- #container -->
<div class="container-fluid location-gallery">
	<main>
		<div class="gallery">
			<?php
			    foreach ( get_field ( 'gallery' ) as $nextgen_gallery_id ) :
			        if ( $nextgen_gallery_id['ngg_form'] == 'album' ) {
			            echo nggShowAlbum( $nextgen_gallery_id['ngg_id'] ); //NextGEN Gallery album
			        } elseif ( $nextgen_gallery_id['ngg_form'] == 'gallery' ) {
			             echo nggShowGallery( $nextgen_gallery_id['ngg_id'] ); //NextGEN Gallery gallery
			        }
			    endforeach;
			?>
		</div>
	</main>
</div>
<?php get_footer('static'); ?>