
<div class="latlong-404">
	<svg id="svg"><defs></defs></svg>
	<div id="latlong-form">
		<form>
			<label for="latitude">Latitude</label>
			<input id="latitude" type="text" placeholder="41°34'21.43&quot;S">
			<label for="longitude">Longitude</label>
			<input id="longitude" type="text" placeholder="174°47'48.93&quot;E">
			<input type="hidden" name="action" value="checkLatLong"/>
			<button id="submit" type="submit">Click to Enter</button>
			<script type="text/javascript">
				var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
			</script>
		</form>
				<div class="banner">
					<h1>404</h1>
					<hr>
					<h2>Page not found</h2>
				</div>
	</div>
</div>
