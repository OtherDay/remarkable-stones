<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package Remarkable Stones
 */
?>

<div class="container-fluid">
<div class="row">
	<div class=" col-xs-12 col-md-4 col-md-offset-4">
		<?php wp_nav_menu( array( 'theme_location' => 'primary') ); ?>
	</div>
	<div class="col-xs-12 col-md-4">
		<ul class="sm-links">
			<li>
				<?php echo do_shortcode('[ssba]'); ?>
			</li>
		</ul>
	</div>
</div>

	<div id="footer">
	<?php wp_footer(); ?>
	<footer class="static-site-footer" role="contentinfo">
		<div class="site-info">
				<p>©copyright Remarkable Stones 2014</p>
		</div>
	</footer>
	</div>
</div>

</body>
</html>
