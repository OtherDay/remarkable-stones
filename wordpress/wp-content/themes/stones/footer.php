<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package Remarkable Stones
 */
?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="site-info">
			<a href="<?php echo esc_url( __( 'http://wordpress.org/', 'stones' ) ); ?>"><?php printf( __( 'Proudly powered by %s', 'stones' ), 'WordPress' ); ?></a>
			<span class="sep"> | </span>
			<?php printf( __( 'Theme: %1$s by %2$s.', 'stones' ), 'Remarkable Stones', '<a href="http://otherday.net" rel="designer">Jonas Ermen</a>' ); ?>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->
</div> <!-- #container -->
<?php wp_footer(); ?>

</body>
</html>
