<?php
/**
 * Remarkable Stones functions and definitions
 *
 * @package Remarkable Stones
 */


/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 640; /* pixels */
}

if ( ! function_exists( 'stones_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function stones_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Remarkable Stones, use a find and replace
	 * to change 'stones' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'stones', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	//add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'stones' ),
		'location' => __( 'Location Menu', 'stones' )
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See http://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'quote', 'link',
	) );

	// Setup the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'stones_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif; // stones_setup
add_action( 'after_setup_theme', 'stones_setup' );

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function stones_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Stones', 'stones' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
}
add_action( 'widgets_init', 'stones_widgets_init' );



// // Register the script first.
// wp_register_script( 'check-lat-long', get_template_directory_uri() . '/js/latlong-query.js' );

// // Now we can localize the script with our data.
// $translation_array = array( 'some_string' => __( 'Some string to translate' ), 'a_value' => '10' );
// wp_localize_script( 'some_handle', 'object_name', $translation_array );

/**
 * Enqueue scripts and styles.
 */
function stones_scripts() {
	wp_enqueue_style( 'stones-style', get_stylesheet_uri() );

	// wp_enqueue_script('jquery');

	wp_enqueue_script( 'stones-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

	wp_enqueue_script( 'stones-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20120206', true );

	wp_enqueue_script( 'stones-map', get_template_directory_uri() . '/js/map.js', array('google-maps-api', 'jquery'), false, true );

	wp_enqueue_script( 'maskedinput', get_template_directory_uri() . '/js/maskedinput.js', array('jquery'), '1.3.1', true );
	
	wp_enqueue_script( 'inputmask', get_template_directory_uri() . '/js/inputmask.js', array('jquery', 'maskedinput'), false, true );

	wp_enqueue_script( 'check-lat-long', get_template_directory_uri() . '/js/latlong-query.js', array('jquery'), false, true );
	
	wp_enqueue_script( 'sweet-alert', get_template_directory_uri() . '/js/sweet-alert.min.js', array(), false, true );

	wp_enqueue_script( 'bookmark', get_template_directory_uri() . '/js/bookmark.js', array('jquery'), false, true );

	wp_enqueue_script( 'snap', get_template_directory_uri() . '/js/snap.svg.js', array('jquery'), false, true );
	
	wp_enqueue_script( 'latlong-animation', get_template_directory_uri() . '/js/latlong-animation.js', array('jquery', 'snap'), false, true );

	wp_enqueue_script( 'google-maps-api', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyCyyRGtTmX5ZJb0MBJ_Pgr-TmpCRIxBfKw', array(), false, true );
	
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'stones_scripts' );



function latlong_query(){

	// global $wpdb; // this is how you get access to the database // or is it??
	$latitude_query = $_POST['latitude'];
	$longitude_query = $_POST['longitude'];

	// args
	$args = array(
		// 'numberposts' => -1,
		'post_type' => 'location',
		'meta_query' => array(
			array(
				'key' => 'latitude',
				'value' => $latitude_query
			),
			'relation' => 'AND',
			array(
				'key' => 'longitude',
				'value' => $longitude_query
			),
		)
	);

	// get results
	$the_query = new WP_Query( $args );

	if ( $the_query->have_posts() ) {
		while ( $the_query->have_posts() ) {

			$the_query->the_post();
			echo get_the_permalink();
			die(); 
		}
	} 

	wp_reset_query();  // Restore global post data stomped by the_post().
}
add_action('wp_ajax_latlong_query', 'latlong_query');
add_action('wp_ajax_nopriv_latlong_query', 'latlong_query');



// Register Custom Post Type
function locations_post_type() {

	$location_labels = array(
		'name'               => 'Locations',
		'singular_name'      => 'Location',
	);

	$args = array(
		'labels'             => $location_labels,
		'public'             => true,
		'menu_position'      => 5,
		'menu_icon'      	 => 'dashicons-format-image',
		'supports'           => array( 'title', 'editor'),
	);

	register_post_type( 'location', $args );

}

// Hook into the 'init' action
add_action( 'init', 'locations_post_type', 0 );




/**
 * Implement the Custom Header feature.
 */
//require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';
