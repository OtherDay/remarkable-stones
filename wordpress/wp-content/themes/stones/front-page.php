<?php
/**
 * The template for displaying the front page.
 *
 * @package Remarkable Stones
 */
?>

<?php get_header( "static"); ?>

<div class="ruler clearfix"></div>

<div class="container-fluid">

<?php get_template_part( 'landing-slide' ); ?>

</div> <!-- #container -->
				
<?php get_footer('static'); ?>
