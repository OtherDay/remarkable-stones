(function($){

   // $.mask.definitions['E']='[EWew]';
   // $.mask.definitions['S']='[SNsn]';

   $("#latitude").mask("?99°99'99.99\"S",{placeholder:"_"});
   $("#longitude").mask("?999°99'99.99\"E",{placeholder:"_"});
})(jQuery);