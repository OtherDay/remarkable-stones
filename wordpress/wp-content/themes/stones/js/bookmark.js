(function($){
	$(function() {
	    $("#bookmark").click(function() {
	    	if( /*@cc_on!@*/false) { // IE Favorite
	            window.external.AddFavorite(location.href,document.title); 
	        } else { 
	            swal({       
	            	title: document.title,
					text: 'Press ' + (navigator.userAgent.toLowerCase().indexOf('mac') != - 1 ? 'Command/Cmd' : 'CTRL') + ' + D to bookmark this page.',     
					type: "warning",     
					confirmButtonText: "Dismiss"   
				});
	        }
	    });
	});
})(jQuery);