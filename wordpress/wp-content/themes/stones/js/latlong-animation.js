(function($){
var LatLong = function(element, color, outTime, inTime, animation, animationIn){

   	var latlong = Snap(element);
	latlong.attr({
   		viewBox: "0 0 500 500"
   	});

	var up = latlong.path("M 245 144 H 255 L 250 70Z").attr({
	        fill: color,
	        stroke: color,
	        strokeWidth: .5
	    });
	var down = latlong.path("M 245 356 H 255 L 250 410Z").attr({
	        fill: color,
	        stroke: color,
	        strokeWidth: .5
	    });
	var left = latlong.path("M 144 245 V 255 L 90 250Z").attr({
	        fill: color,
	        stroke: color,
	        strokeWidth: .5
	    });
	var right = latlong.path("M 356 245 V 255 L 410 250Z").attr({
	        fill: color,
	        stroke: color,
	        strokeWidth: .5
	    });

	var n = latlong.text(250,65, "N").attr({
	        fill: color,
	        // 'fill-opacity': 0.0,
			"font-family": "MuseoSans",
			"text-anchor": "middle"
	});

	$(document).ready(function(){

		$("#latlong-form").mouseover(function(){
			up.stop();
			down.stop();
			left.stop();
			right.stop();

			n.animate( { "transform" : "t-0 -30"}, outTime, mina[animation]);

			up.animate({d:"M 245 124 H 255 L 250 50Z"}, outTime, mina[animation]);
			down.animate({d:"M 245 376 H 255 L 250 430Z"}, outTime, mina[animation]);
			left.animate({d:"M 124 245 V 255 L 70 250Z"}, outTime, mina[animation]);
			right.animate({d:"M 376 245 V 255 L 430 250Z"}, outTime, mina[animation]);
		});
		$("#latlong-form").mouseout(function(){
			n.stop();
			up.stop();
			down.stop();
			left.stop();
			right.stop();
			n.animate( { "transform" : "t-0 0" }, inTime, mina[animationIn]);
			up.animate({d:"M 245 144 H 255 L 250 70Z"}, inTime, mina[animationIn]);
			down.animate({d:"M 245 356 H 255 L 250 410Z"}, inTime, mina[animationIn]);
			left.animate({d:"M 144 245 V 255 L 90 250Z"}, inTime, mina[animationIn]);
			right.animate({d:"M 356 245 V 255 L 410 250Z"}, inTime, mina[animationIn]);
		});
	});
}

if ( $("#svg").length) {
	new LatLong("#svg", "#fff", 1500, 500, 'bounce', 'backout');
};

})(jQuery);