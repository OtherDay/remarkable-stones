(function($) {

$(document).ready(function(){


	$("#submit").click(function(e){

		e.preventDefault();

		var latField = $('#latitude');
		var qLat = latField.val();
		var cqLat = convertLatLong(qLat, "lat");

		var longField = $('#longitude');
		var qLong = longField.val();
		var cqLong = convertLatLong(qLong, "long");

		var data = {
				'action': 'latlong_query', 
				'latitude': cqLat,
				'longitude': cqLong
			};

		var url = "./wp-admin/admin-ajax.php"

		var settings = {
			type: "POST",
			url: ajaxurl,
			data: data, 
			success: function(response) {
				if (response === '0') {
					// alert("No stone found at \nlat: " + qLat + "\nlong: " + qLong);
					swal({
						title: "",
						type: "info",      
						text: "Sorry, we don't appear to have a match with the co-ordinates you have entered. Please try again, or contact us using our feedback form on the contacts page.",
						allowOutsideClick: true,
						confirmButtonText: " "
					});
				} else {					
					$(location).attr('href',response);
				}
			}
		};

		$.ajax(settings);

	}); // # .click



}); // # $(document).ready


function convertLatLong (latlong, mode) {
	var tempStr = "";
	
	// console.log(latlong);

	for (var i = 0; i < latlong.length; i++) {
		if (latlong[i].indexOf('°') !== -1) {
			tempStr += ".";
		} else if (!isNaN(parseInt(latlong[i]))) {
			tempStr += latlong[i];
		}
	}
	if (tempStr.indexOf('.') !== -1) {

		var decimals = tempStr.split(".")[1].length;

		for (var i = 0; i < 6 - decimals; i++) {
			tempStr += "0";
		};
	};

	if (mode === "lat") {
		tempStr = "-" + tempStr;
	};
	
		// console.log('send:\n' + tempStr);
	return tempStr;		
}

})(jQuery);