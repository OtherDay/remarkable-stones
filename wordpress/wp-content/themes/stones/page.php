<?php
/**
 * The template for displaying all stone posts.
 *
 * @package Remarkable Stones
 */

get_header('static'); ?>

<div class="ruler clearfix"></div>

<div class="img-responsive header-image pos-rel">
	<img src="<?php echo get_field('main-image')['url'] ?>">
	<div class="shadow-left"></div>
	<div class="shadow-right"></div>
</div>

<div class="container-fluid about-body">

	<div class="row">
		<div class="col-xs-12 col-md-7">
			
			<main>
				<div class="about-banner">
					<h2>About</h2>
				</div>

				<?php while ( have_posts() ) : the_post(); ?>
					<?php the_content(); ?>	
				<?php endwhile; // end of the loop. ?>	
				<div class="about-image">
					<img src="<?php echo get_field('about_image')['url'] ?>">
				</div>
			</main>
		</div> <!-- #col -->
		<div class="col-xs-12 col-md-5">
			<div class="about-sidebar">
				<div class="about-banner">
					<h2>Contact</h2>
				</div>
				<p><?php echo get_field('above_contact_form_text') ?></p>
				<?php echo do_shortcode('[contact-form-7 id="100" title="Contact form 1"]'); ?>
			</div>
		</div> <!-- #sidebar col -->
	</div> <!-- #row -->
</div> <!-- #container -->

<?php get_footer('static'); ?>


