
<div class="row landing-slide border-solid-bottom">
	<div class="hidden-xs hidden-sm col-md-4 no-padding pos-rel border-right-dotted-1">
		<div class="shadow-left hidden-xs hidden-sm"></div>
		<?php do_action('slideshow_deploy', 'left-1'); ?>
	</div>
	<div class="col-xs-12 col-md-4 no-padding pos-rel border-right-dotted-1">
		<div class="latlong-overlay">
			<?php get_template_part( 'latlong-input' ); ?>
		</div>
		<?php do_action('slideshow_deploy', 'center-1'); ?>
	</div>
	<div class="hidden-xs hidden-sm col-md-4 no-padding pos-rel border-right-1">
		<div class="shadow-right hidden-xs hidden-sm"></div>
		<?php do_action('slideshow_deploy', 'right-1'); ?>
	</div>
</div>