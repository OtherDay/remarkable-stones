<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package Remarkable Stones
 */
?>


<footer class="location-site-footer" role="contentinfo">
	<div class="site-info">
		<p><span id="bookmark" onclick="window.external.AddFavorite(location.href, document.title);">Bookmark Page</span></p>
		<p>©copyright Remarkable Stones 2014</p>
	</div><!-- .site-info -->
</footer><!-- #colophon -->
<?php wp_footer(); ?>
</body>
</html>
