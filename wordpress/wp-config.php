<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'remarkable_stones');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'iviuen%W{W!|<8dDwY@_*nrDnXwbMhd&Tw8NP`DAA}apeoexHA?V-ej!iU}dB:kc');
define('SECURE_AUTH_KEY',  'k]:<0}HSE8L9`wPzwN0tyIEm-O(u)d?NXBz}Au7w!>MSK~cM=zH/aj@jp|Spi#R*');
define('LOGGED_IN_KEY',    'VP`gdG;980*Su4qI`]id6AoNFm9g362qDab(<98<ovVZ!9aA9ZoeXA v1.*9MuHj');
define('NONCE_KEY',        'rhAmLx^pa)whcC{UG1zQQ}:!Xo$^F4fMg.ml8GQA#B!;wtrDd:@Ur5v1lw6pmu0a');
define('AUTH_SALT',        '~0u/b~a{TpBT(z`.-=]U+6vP_-@wgbCLOc9-.@`>(/cv3tu~,xqwrrjldxej8k|E');
define('SECURE_AUTH_SALT', 'PHgn0SGYh3_4oXEEx47.jG+CO:bp/U,gB0xV)uqqcGArhxO_jjzr}9/Fb|<@}5B3');
define('LOGGED_IN_SALT',   'T^ND(rc6d9mj!)6n5lwhcWF=sA/x8v6=Qerd6(#8ChAGRSLF,!eeE$!fJ?ssU$b0');
define('NONCE_SALT',       'J1b68wj>N*4em1kd&aFGqGHg D4&?imN+x:L{dk)YrL&K/7G42xT#2(o-}43}VWn');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
